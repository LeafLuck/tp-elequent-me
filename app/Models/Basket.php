<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    use HasFactory;
}
class Commande extends Model
{
    use HasFactory;
    protected $tableName = "commandes";
    protected $fillable= [
        'client_id',
        'date_time',
        'description',
        'prix_total'
    ];
}
