<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produit;

class BasketController extends Controller
{
    public function index(Request $request)
    {
      
        $items = $request->session()->get('basket', []);
    
        return view('basket.index', compact('items'));
    }
    

    
    public function addToBasket(Request $request, $product)
{
    
    $produit = Produit::findOrFail($product);

    
    $item = [
        'designation' => $produit->designation ?? 'N/A',
        'prix_u' => $produit->prix_u ?? 'N/A',
        'photo' => $produit->photo ?? 'N/A',
    ];

    // Add 
    $request->session()->push('basket', $item);

    return redirect()->route('basket.index');
}
public function clear(Request $request ){

    $request->session()->forget('basket');
    // return redirect()->back();
    return redirect()->route('basket.index');

}

public function commander(Request $request){
        $panier=$request->session()->get('panier',[]);
        $tot=0;
        
        
        foreach($panier as $id=>$item){
            $tot+=$item['qte']*$item['produit']->prix_u;
            
        }
        
        
        return view('basket.commander',compact('tot'));
    }  
    
    public function storeClientCommande(Request $request){
        
        //Categorie::create($request->all());
        $clientdata = $request->validate([
         'nom'=>'required',
         'prenom'=>'required',
         'tele'=>'required',
         'ville'=>'required',
         'adresse'=>'required'
     ]);
     
        
         $client = Client::create($clientdata);
     
     $client_id = $client->id;
     $client_name = $client->nom . ' ' . $client->prenom;
     $date_time = now();
     $commandedata=$request->validate([
         'description'=>'required',
         'prix_total'=>'required'
     ]);
     $commandedata['client_id'] = $client_id;
     $commandedata['date_time'] = $date_time;
     Commande::create($commandedata);
     return view('basket.index');
       
     
     
     
 
     }  


}