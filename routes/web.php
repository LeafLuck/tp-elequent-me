<?php

use App\Http\Controllers\CategorieController;
use App\Http\Controllers\ProduitController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BasketController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('accueil');
Route::get('/categories/recherche', [CategorieController::class, 'recherche'])->name('categories.recherche');

Route::get('/', [ProduitController::class, 'indexp'])->name('welcome');

Route::resource("categories",CategorieController::class);
Route::resource("produits",ProduitController::class);
Route::prefix('basket')->group(function () {
    Route::get('/', [BasketController::class, 'index'])->name('basket.index');
    Route::post('/remove/{product}', [BasketController::class, 'removeFromBasket'])->name('basket.remove');});
    Route::post('/add-to-basket/{product}', [BasketController::class, 'addToBasket'])->name('basket.add');
    Route::get('/basket/destroy', [BasketController::class, 'destroyBasket'])->name('basket.destroy');
    Route::get('/basket/commander',[BasketController::class,"commander"])->name('basket.commander');

        