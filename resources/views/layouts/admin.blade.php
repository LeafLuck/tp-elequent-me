<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <title>@yield('title','App store')</title>
</head>
<body>
    <nav>
        <ul>
             <li><a href="{{route('welcome')}}">Accueil</a></li> 
            <li><a href="{{route('categories.index')}}">Gestion des categories</a></li>
            <li><a href="{{route('produits.index')}}">Gestion des produits</a></li>
            <li><a href="{{route('basket.index')}}">Mon panier</a></li>

        </ul>
    </nav>
    <div class="main">
        @yield('content')
    </div>
    <footer>
        &copy;OFPPT 2024
    </footer>
</body>
</html>
