


@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')
<style>
    .order-validation-container {
    max-width: 600px;
    margin: auto;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 8px;
    background-color: #fff;
}

.form-group {
    margin-bottom: 15px;
}

label {
    display: block;
    font-weight: bold;
    margin-bottom: 5px;
}

input,
textarea {
    width: 100%;
    padding: 8px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 5px;
}

button.btn-submit {
    background-color: #4caf50;
    color: #fff;
    padding: 10px 15px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

button.btn-submit:hover {
    background-color: #45a049;
}

</style>
    <div class="order-validation-container">
        <h1>Validation de la Commande</h1>

        <form method="post" action="/">
            @csrf

            <!-- Client information fields -->
            <div class="form-group">
                <label for="nom">Nom:</label>
                <input type="text" name="nom" required>
            </div>

            <div class="form-group">
                <label for="prenom">Prenom:</label>
                <input type="text" name="prenom" required>
            </div>

            <div class="form-group">
                <label for="tele">Telephone:</label>
                <input type="text" name="tele" required>
            </div>

            <div class="form-group">
                <label for="ville">Ville:</label>
                <input type="text" name="ville" required>
            </div>

            <div class="form-group">
                <label for="adresse">Adresse:</label>
                <textarea name="adresse" rows="4" required></textarea>
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" rows="8" cols="100"></textarea>
            </div>

            <div class="form-group">
                <label for="prix_total">Prix total:</label>
                <input type="text" name="prix_total" value="{{ $tot }}" readonly>
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn-submit">Commander</button>
        </form>
    </div>
@endsection
