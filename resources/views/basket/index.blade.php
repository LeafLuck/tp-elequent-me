@extends('layouts.admin')
@section('title', 'Detail d \'une produit')
@section('content')
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<section class="h-100 gradient-custom">
    <div class="container py-5">
        <div class="row d-flex justify-content-center my-4">
            <div class="col-md-8">
                <div class="card mb-4">
                    <div class="card-header py-3">
                        <h5 class="mb-0">Cart </h5>
                    </div>
                    @if(count($items) > 0)
                    @foreach($items as $item)
                    <div class="card-body">
                        @if (is_array($item) || is_object($item))
                        <div class="row">
                            <div class="col-lg-3 col-md-12 mb-4 mb-lg-0">
                                <!-- Image -->
                                <div class="bg-image hover-overlay hover-zoom ripple rounded"
                                    data-mdb-ripple-color="light">
                                    @if (isset($item['photo']) || isset($item->photo))
                                    <img src="{{ asset('storage/' . ($item['photo'] ?? $item->photo ?? 'N/A')) }}"
                                        alt="{{ $item['designation'] ?? 'Product' }}" width="100px" height="100px">
                                    @else
                                    <p>No photo available</p>
                                    @endif
                                    <a href="#!">
                                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.2)"></div>
                                    </a>
                                </div>
                                <!-- Image -->
                            </div>

                            <div class="col-lg-9 col-md-12">
                                <!-- Data -->
                                <p><strong>{{ $item['designation'] ?? $item->designation ?? 'N/A' }}</strong></p>
                                <p>Color: Black</p>

                                <div class="d-flex mb-4 " style="max-width: 30px">
                                    <button class="btn btn-primary px-3 me-2"
                                        onclick="this.parentNode.querySelector('input[type=number]').stepDown()">
                                        <i class="fa-solid fa-minus"></i>
                                    </button>

                                    <div class="form-outline">
                                        <input id="form1" min="0" name="quantity" value="1" type="number"
                                            class="form-control" />
                                        <label class="form-label" for="form1">Quantity</label>
                                    </div>

                                    <button class="btn btn-primary px-3 ms-2"
                                        onclick="this.parentNode.querySelector('input[type=number]').stepUp()">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>

                                <p class="text-start text-md-center ">
                                    <strong>{{ $item['prix_u'] ?? $item->prix_u ?? 'N/A' }} MAD</strong>
                                </p>

                                <button type="button" class="btn btn-primary btn-sm me-1 mb-2"
                                    data-mdb-toggle="tooltip" title="Remove item">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-sm mb-2" data-mdb-toggle="tooltip"
                                    title="Move to the wish list">
                                    <i class="fas fa-heart"></i>
                                </button>
                                <!-- Data -->
                            </div>
                        </div>
                        @else
                        {{-- Handle other item formats --}}
                        <p>Invalid item format: {{ json_encode($item) }}</p>
                        @endif
                    </div>
                    @endforeach
                    @else
                    <p class="mt-3">Your basket is empty.</p>
                    @endif
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <p><strong>Expected shipping delivery</strong></p>
                        <p class="mb-0">12.10.2020 - 14.10.2020</p>
                    </div>
                </div>

                <div class="card mb-4 mb-lg-0">
                    <div class="card-body">
                        <p><strong>We accept</strong></p>
                        <img class="me-2" width="45px"
                            src="https://mdbcdn.b-cdn.net/wp-content/plugins/woocommerce-gateway-stripe/assets/images/visa.svg"
                            alt="Visa" />
                        <img class="me-2" width="45px"
                            src="https://mdbcdn.b-cdn.net/wp-content/plugins/woocommerce-gateway-stripe/assets/images/amex.svg"
                            alt="American Express" />
                        <img class="me-2" width="45px"
                            src="https://mdbcdn.b-cdn.net/wp-content/plugins/woocommerce-gateway-stripe/assets/images/mastercard.svg"
                            alt="Mastercard" />
                        
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-header py-3">
                        <h5 class="mb-0">Summary</h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li
                                class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                Products
                                <span>$53.98</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                Shipping
                                <span>Gratis</span>
                            </li>
                            <li
                                class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                <div>
                                    <strong>Total amount</strong>
                                    <strong>
                                        <p class="mb-0">(including VAT)</p>
                                    </strong>
                                </div>
                                <span><strong>$53.98</strong></span>
                            </li>
                        </ul>

                        <button  >
                       <a type="button" class="btn btn-primary btn-lg btn-block"  href="{{ route('basket.commander') }}">Go checkout</a>     
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
