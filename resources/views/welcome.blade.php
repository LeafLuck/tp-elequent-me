@extends('layouts.admin')
 @section('title','Detail d \'une produit')
 @section('content')
<div class="container mx-auto mt-4">
  
    <div class="row">
        @foreach ($produits as $pro)
        <div class="card m-3" style="width: 18rem;">
            <img src="{{ asset('storage/' . $pro->photo) }}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Prix : {{$pro->prix_u}}</h5>
              
             {{--  --}}
              <p class="card-text">{{$pro->designation}}</p>
              <a href="{{route('produits.show',['produit'=>$pro->id])}}" class="btn btn-primary">Details</a>
              <form action="{{ route('basket.add', ['product' => $pro->id]) }}" method="POST">
        @csrf
        <button type="submit">Add to Basket</button>
    </form>
            </div>
          </div>
      @endforeach    
          
           
  </div>
    </div>
    @endsection